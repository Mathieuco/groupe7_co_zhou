<?php 
    session_start();
    // on se connecte à la bdd
    include "conf_inscription.php";
    // on vérifie que l'id_client existe
    if(isset($_GET['id_client']) AND !empty($_GET['id_client'])){
        $getid = $_GET['id_client'];
        $recupUser = $bdd->prepare('SELECT * FROM client WHERE id_client = ?');
        $recupUser -> execute(array($getid));
        if($recupUser -> rowCount() > 0){
            $deleteUser = $bdd->prepare('DELETE FROM client WHERE id_client= ?');
            $deleteUser -> execute(array($getid));

            header('Location: utilisateur.php');
        }
        else{
            echo "Aucun utilisateur n'a été trouvé";
        }
    }
    else{
        echo "L'identifiant n'a pas été récupéré";
    }
?>