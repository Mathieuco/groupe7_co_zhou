<?php 
    // on se connecte à la bdd
    include "conf_inscription.php";

    // on vérifie que l'id_client existe
    if(isset($_GET['id_client']) AND !empty($_GET['id_client'])){
        $getid = $_GET['id_client'];
        $recupUser = $bdd->prepare('SELECT * FROM client WHERE id_client = ?');
        $recupUser -> execute(array($getid));

        // si l'id a bien été récupérer
        if($recupUser -> rowCount() > 0){
            // récupère les données à modifier
            $userInfo = $recupUser -> fetch();
            $identifiant = $userInfo['identifiant'];
            $role = $userInfo['role'];

            // après validation en appuyant sur le bouton valider
            if(isset($_POST['valider'])){
                // empécher de mettre du code html dans l'identifiant
                $identifiant_saisi = htmlspecialchars($_POST['identifiant']);
                $role_saisi = $_POST['role'];

                // requete SQL pour modifier l'identifiant et/ou le role 
                $update = $bdd -> prepare('UPDATE client SET identifiant = ?, role = ? WHERE id_client = ?');
                $update -> execute(array($identifiant_saisi, $role_saisi, $getid));

                echo "Modification des données réussis";
                header( 'Location: utilisateur.php');
            }

        }
        else{
            echo "Aucun identifiant n'a été trouvé";
        }
    }
    else{
        echo "L'id n'a pas été récupéré";
    }
?>

<!DOCTYPE html>

<html>
    <head>
        <?php include 'header.php';?>
    </head>

    <body>

        <?php include 'menu.php';?>
        <form class="box" action="" method="post">

            <h1 class="box-title">Modifier les données d'un utilisateur</h1>

                <input type="text" class="box-input" name="identifiant" placeholder="Nom d'utilisateur" value="<?= $identifiant ?>" required /> <br>
            
                <select name="role" id="role">
                    <option value="<?= $role ?>">Role</option>
                    <option value="0">0</option>
                    <option value="1">1</option>
                </select>
                <br>
            
                <input type="submit" name="valider" value="Valider" class="box-button" />
            
        </form>
        
    </body>
    <?php
        include 'footer.php'; 
    ?>
</html>