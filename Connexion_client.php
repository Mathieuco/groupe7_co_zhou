<!DOCTYPE html>

<html>
    <head>
        <!-- importer le fichier de style  -->
        <?php
            include 'header.php';
        ?>
        <link rel="stylesheet" href="style_client.css" type="text/css" media="screen" charset="utf-8">
    </head>

    <body style='background:#fff;'>
        <div id="content">
            <?php 
                include 'menu.php';
            ?>
            
            <!-- tester si l'utilisateur est connecté  -->
            <h1> connexion réussi </h1>

            <?php
                    
                if(!isset($_SESSION['identifiant']) || $_SESSION['role']!=0){
                    header('Location: Connexion_client.php');
                }
                else{                   
                // afficher un message
                echo "Bonjour " .$_SESSION['identifiant']. ", vous êtes connecté en tant que ".$_SESSION['role'] ;
                }
                
            ?>     
            
            <form>
                <br>
                <button type="submit" formaction="client_commander.php">Faire une commande</button>
                <br>
            </form>
            <form>
                <br>
                <button type="submit" formaction="client_voir.php">Voir commande</button>
                <br>
            </form>

        </div>
        
    </body>
    <?php
        include 'footer.php'; 
    ?>
</html>