<!DOCTYPE html>

<html>
    <head>
        <?php
            include 'header.php';
        ?>        
        <link rel="stylesheet" href="style_con.css" media="screen" type="text/css" charset="utf-8"/>
    </head>

    <body>
        <?php include 'menu.php'; ?>
        <div id="container">
            <!-- zone de connexion -->
            
            <form class="box" action="conf_connexion_client.php" method="POST">
            <h1>Connexion</h1>
            
            <label><b>Identifiant</b></label>
            <input type="text" placeholder="Entrer l'identifiant" name="identifiant" required>

            <label><b>Mot de passe</b></label>
            <input type="password" placeholder="Entrer le mot de passe" name="mot_de_passe" required>

            <input type="submit" id='submit' name='login' value='LOGIN' >

            <p class="box-register"> Pas encore inscrit? <a href="inscription.php"> Inscrivez-vous ici </a> </p>
            </form>
            
        </div>

        
    </body>
    <?php
        include 'footer.php'; 
    ?>
</html>