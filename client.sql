-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Ven 25 Novembre 2022 à 06:17
-- Version du serveur :  5.7.11
-- Version de PHP :  5.6.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `bdd_6_7`
--

-- --------------------------------------------------------

--
-- Structure de la table `client`
--

CREATE TABLE `client` (
  `id_client` int(11) NOT NULL,
  `identifiant` varchar(50) NOT NULL,
  `mot_de_passe` varchar(110) NOT NULL,
  `id_commande` int(11) DEFAULT NULL,
  `role` int(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `client`
--

INSERT INTO `client` (`id_client`, `identifiant`, `mot_de_passe`, `id_commande`, `role`) VALUES
(1, 'a', '$2y$12$8mP6DybE9RpRe1LIREjBbuGEGtFuFv.hxQN70t3j5AiHz6OkzbYbG', NULL, 1),
(2, 'b', '$2y$12$dKUwikT1S.tAk1MAGIMhz.c.cYUK8uLNOrK3sRzzt0vQDdTeH5oo6', NULL, 1),
(3, 'c', '$2y$12$zi2H4OygDyGwKfDcjrLTyOdlo8MDhxv7.dGUgZEMZR5c2t8ED4VEa', NULL, 0),
(4, 'd', '$2y$12$Jpni6s3xGa8uYqqNPNmM9e465IrVaEkeQ/Lesh.gQgOEC8W.d3WfC', NULL, 0),
(5, 'marc', '$2y$12$FITrewhZmnbP0eqnWXE6guNqYsom1/kTsXNzVKzPWXdrhuavodHNy', NULL, 1);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`id_client`),
  ADD KEY `id_client` (`id_client`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `client`
--
ALTER TABLE `client`
  MODIFY `id_client` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
