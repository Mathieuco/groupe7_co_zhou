<?php 
    session_start();
    // on se connecte à la bdd
    include "conf_inscription.php";
    // on vérifie que l'nom existe
    if(isset($_GET['nom']) AND !empty($_GET['nom'])){
        $getid = $_GET['nom'];
        $recupUser = $bdd->prepare('SELECT * FROM plat WHERE nom = ?');
        $recupUser -> execute(array($getid));
        if($recupUser -> rowCount() > 0){
            $deleteUser = $bdd->prepare('DELETE FROM plat WHERE nom= ?');
            $deleteUser -> execute(array($getid));

            header('Location: plats.php');
        }
        else{
            echo "Aucun plat n'a été trouvé";
        }
    }
    else{
        echo "Le nom du plat n'a pas été récupéré";
    }
?>