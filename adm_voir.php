<!DOCTYPE html>

<html>
    <head>
        <?php include 'header.php';?>
        <link rel="stylesheet" href="style_administrateur.css" type="text/css" media="screen" charset="utf-8">
    </head>

    <body>
        <?php
                include 'menu.php';
                    
                if(!isset($_SESSION['identifiant']) || $_SESSION['role']!=1){
                    header('Location: page_administrateur.php');
                }
                else{                   
                // afficher un message
                echo "Bonjour " .$_SESSION['identifiant']. ", vous êtes connecté en tant que ".$_SESSION['role'] ;
                }
                
            ?>
        <table>
                <caption>Liste des commandes</caption>
                <tr > <th>numéro de la commande</th>   <th>Entrée</th>  <th>Plat</th>  <th>Dessert</th>  <th>Prix</th>  <th>Etat</th> <th>Utilisateur</th>
                <th>Horraire de retrait</th> </tr>
                <?php
                  
                        include "conf_inscription.php";
                        $idClient=$_SESSION['id_client'];
                        $recupCom = $bdd->query("SELECT * FROM commander");
                        while($com = $recupCom->fetch()){
                            ?>
                            <tr> <p> <th><?= $com['id_commande']; ?></th> <th><?= $com['entrée_c']; ?></th> <th><?= $com['plat_c']; ?></th> 
                            <th><?= $com['dessert_c']; ?></th> <th><?= $com['prix_c']; ?></th> <th><?= $com['etat']; ?></th> <th><?= $com['id_client']; ?></th> <th><?= $com['date']; ?></th>
                            
                                <th><a href="client_supprimer.php?id_commande=<?= $com['id_commande']; ?>" style="color:
                                red; text-decoration: none;">   Supprimer la commande</a></th> 
                                <th><a href="client_modifier.php?id_commande=<?= $com['id_commande']; ?>" style="color:
                                green; text-decoration: none;">   Modifier la commande</a></th> </p> </tr> 
                                <?php
                                                   
                            
                        }
                    
                        
                ?>
        </table>
            
    </body>
    <?php include 'footer.php';?>

</html>