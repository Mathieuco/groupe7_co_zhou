<!DOCTYPE html>

<html>
    <head>
        <?php include 'header.php';?>
    </head>

    <body>

        <?php include 'menu.php';?>
        <form class="box" action="" method="post">
            <h1 class="box-logo box-title"></h1>

            <h1 class="box-title">S'inscrire</h1>

                <input type="text" class="box-input" name="identifiant" placeholder="Nom d'utilisateur" required />
            
                <input type="password" class="box-input" name="mot_de_passe" placeholder="Mot de passe" required />

                <input type="password" class="cbox-input" name="cmot_de_passe" placeholder="Confirmer votre mot de passe" required />

                <?php 
                    if(isset($_SESSION['identifiant']) && ($_SESSION['role']==1)){
                        ?>
                        <select name="role" id="role">
                            <option value="">Role</option>
                            <option value="0">0</option>
                            <option value="1">1</option>
                        </select> <?php
                    }
                ?>
            
                <input type="submit" name="submit" value="S'inscrire" class="box-button" />
            
                <p class="box-register">Déjà inscrit? <a href="Page_connexion_client.php"> Connectez-vous ici </a> </p>
        </form>

        <?php
            if(isset($_SESSION['identifiant']) && ($_SESSION['role']==1)){
                require('conf_inscription.php');
                if(isset($_POST['submit'])){

                    extract($_POST);

                    if(!empty($mot_de_passe) && !empty($cmot_de_passe) && !empty($identifiant)){
                
                        if($mot_de_passe == $cmot_de_passe){

                            $option = [
                                'cost' =>12,
                            ];

                            $hashpass = password_hash($mot_de_passe, PASSWORD_BCRYPT, $option);

                            global $bdd;                        

                            $q = $bdd->prepare("INSERT INTO client(identifiant, mot_de_passe, role) VALUES(?, ?,?)");
                            
                            if($q->execute(array($identifiant,$hashpass, $role))){
                                $_SESSION['message'] = "Enregistrement réussi";
                                echo $_SESSION['message']; 
                            }
                            else{
                                $_SESSION['message'] = "impossible d'enregistrer";
                                echo $_SESSION['message'];
                            }
                        }
                    }
                    else{
                        echo "les champs ne sont pas remplies";
                    }
                }
            }
            else{
                require('conf_inscription.php');
                if(isset($_POST['submit'])){

                    extract($_POST);

                    if(!empty($mot_de_passe) && !empty($cmot_de_passe) && !empty($identifiant)){
                
                        if($mot_de_passe == $cmot_de_passe){

                            $option = [
                                'cost' =>12,
                            ];

                            $hashpass = password_hash($mot_de_passe, PASSWORD_BCRYPT, $option);

                            global $bdd;                        

                            $q = $bdd->prepare("INSERT INTO client(identifiant, mot_de_passe, role) VALUES(?, ?,?)");
                            
                            if($q->execute(array($identifiant,$hashpass, 0))){
                                $_SESSION['message'] = "Enregistrement réussi";
                                echo $_SESSION['message']; 
                            }
                            else{
                                $_SESSION['message'] = "impossible d'enregistrer";
                                echo $_SESSION['message'];
                            }
                        }
                    }
                    else{
                        echo "les champs ne sont pas remplies";
                    }
                }
            }
        ?>
        
    </body>

    <?php
        include 'footer.php'; 
    ?>
</html>