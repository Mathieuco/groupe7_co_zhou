-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Ven 25 Novembre 2022 à 06:17
-- Version du serveur :  5.7.11
-- Version de PHP :  5.6.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `bdd_6_7`
--

-- --------------------------------------------------------

--
-- Structure de la table `plat`
--

CREATE TABLE `plat` (
  `nom` varchar(50) NOT NULL,
  `prix` float DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `image` varchar(255) NOT NULL,
  `catégorie` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `plat`
--

INSERT INTO `plat` (`nom`, `prix`, `description`, `image`, `catégorie`) VALUES
('Boeuf Bourguignon', 2, 'Boeuf avec sauce au vin accompagné de carottes et champignons', 'https://img.cuisineaz.com/660x660/2013/12/20/i73725-boeuf-bourguignon.jpg', 'plat'),
('Charcuterie', 2, '', 'https://www.chezmarceline.com/DesktopModules/NBright/NBrightBuy/NBrightThumb.ashx?src=/Portals/0/NBStore/images/TBl9M5WS.png&w=640&h=405', 'entrée'),
('Oeuf mayonnaise', 1, '', 'http://foodandsens.com/wp-content/uploads/2019/06/IMG_1611-1280x720.jpg', 'entrée'),
('Tarte au citron', 1, '', 'https://astucesaufeminin.com/wp-content/uploads/2021/03/01-1.jpeg', 'dessert');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `plat`
--
ALTER TABLE `plat`
  ADD PRIMARY KEY (`nom`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
