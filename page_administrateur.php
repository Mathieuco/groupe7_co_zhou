<!DOCTYPE html>

<html>
    <head>
        <?php
            include 'header.php';
        ?>
        <link rel="stylesheet" href="style_client.css" media="screen" type="text/css" charset="utf-8"/>
    </head>

    <body style='background:#fff;'>
        <div id="content">
            <!-- tester si l'utilisateur est connecté -->
            <?php
                include 'menu.php';
                    
                if(!isset($_SESSION['identifiant']) || $_SESSION['role']!=1){
                    header('Location: page_administrateur.php');
                }
                else{                   
                // afficher un message
                echo "Bonjour " .$_SESSION['identifiant']. ", vous êtes connecté en tant que ".$_SESSION['role'] ;
                }
                
            ?>    
            
            <form>
                <br>
                <button type="submit" formaction="plats.php">Voir plats</button>
                <br>
                <button type="submit" formaction="adm_ajouter_plats.php">Ajouter plats</button>
                <br>
                <button type="submit" formaction="utilisateur.php">Voir des utilisateurs</button>
                <br>
                <button type="submit" formaction="adm_voir.php">Voir les commandes</button>
            </form>
            
        </div>
        
    </body>
    <?php
        include 'footer.php'; 
    ?>
</html>