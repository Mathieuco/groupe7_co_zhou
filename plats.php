<!doctype html>

<html lang="en">
  <head>
    <?php include 'header.php';?>
    <link rel="stylesheet" href="style_administrateur.css" media="screen" type="text/css" charset="utf-8">
  </head>
  
  <body > 
    <!-- bgproperties="fixed" background="https://www.normandie.fr/sites/default/files/2022-05/220506-NicolasBroquedis-0177.jpg" -->
    <?php include 'menu.php';?>      

    <h1>Les plat sont :</h1>

    <br>

    <table>

      <caption>Entrée :</caption>
      <?php
    
      
        $mysqli = new mysqli('localhost','grp_6_7','BP3fo7zi9DEq','bdd_6_7');
        $mysqli->set_charset("utf8");
        $requete = "SELECT * FROM plat WHERE catégorie ='entrée'";
        $resultat = $mysqli->query($requete);

        while ($ligne = $resultat->fetch_assoc()) {
          ?> 
          <tr> <th><?=$ligne['nom']?></th> <th><?=$ligne['prix']?> €</th> <th><img src="<?=$ligne['image']?>" width="128" height="117"></img></th> 
          <th><?=$ligne['description']?></th> 
          <?php

          if(isset($_SESSION['identifiant']) && ($_SESSION['role']==1)){ //si c'est un administrateur il peut supprimer le plat
            ?><th> <a href="adm_supprimer_plat.php?nom=<?= $ligne['nom']; ?>" style="color: red; text-decoration: none;">   Supprimer le plat</a> </th></tr> <?php
          }
          echo '<br>';
        }
      ?>
    
    </table>

    <br><br>

    <table>

      <caption>Plat :</caption>
      <?php

        $requete = "SELECT * FROM plat WHERE catégorie ='plat'";
        $resultat = $mysqli->query($requete);

        while ($ligne = $resultat->fetch_assoc()) {
          ?> 
          <tr> <th><?=$ligne['nom']?></th> <th><?=$ligne['prix']?> €</th> <th><img src="<?=$ligne['image']?>" width="128" height="117"></img></th> 
          <th><?=$ligne['description']?></th> 
          <?php

          if(isset($_SESSION['identifiant']) && ($_SESSION['role']==1)){ //si c'est un administrateur il peut supprimer le plat
            ?><th> <a href="adm_supprimer_plat.php?nom=<?= $ligne['nom']; ?>" style="color: red; text-decoration: none;">   Supprimer le plat</a> </th></tr> <?php
          }
          echo '<br>';
        }
      ?>
    
    </table>

    <br><br>
    <table>

      <caption>Dessert :</caption>
      <?php

        $requete = "SELECT * FROM plat WHERE catégorie ='dessert'";
        $resultat = $mysqli->query($requete);

        while ($ligne = $resultat->fetch_assoc()) {
          ?> 
          <tr> <th><?=$ligne['nom']?></th> <th><?=$ligne['prix']?> €</th> <th><img src="<?=$ligne['image']?>" width="128" height="117"></img></th> 
          <th><?=$ligne['description']?></th> 
          <?php

          if(isset($_SESSION['identifiant']) && ($_SESSION['role']==1)){ //si c'est un administrateur il peut supprimer le plat
            ?><th> <a href="adm_supprimer_plat.php?nom=<?= $ligne['nom']; ?>" style="color: red; text-decoration: none;">   Supprimer le plat</a> </th></tr> <?php
          }
          echo '<br>';
        }
      ?>
    
    </table>
    
  </body>

</html>