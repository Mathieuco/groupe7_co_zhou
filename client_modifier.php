<?php 
    // on se connecte à la bdd
    include "conf_inscription.php";

    // on vérifie que l'id_commande existe
    if(isset($_GET['id_commande']) AND !empty($_GET['id_commande'])){
        $getid = $_GET['id_commande'];
        $recupUser = $bdd->prepare('SELECT * FROM commander WHERE id_commande = ?');
        $recupUser -> execute(array($getid));

        // si l'id a bien été récupérer
        if($recupUser -> rowCount() > 0){
            // récupère les données à modifier
            $userInfo = $recupUser -> fetch();
            $entrée = $userInfo['entrée_c'];
            $plat = $userInfo['plat_c'];
            $dessert = $userInfo['dessert_c'];
            $date = $userInfo['date'];
            $etat = $userInfo['etat'];


            // après validation en appuyant sur le bouton modifier
            if(isset($_POST['modifier_c'])){
                // empécher de mettre du code html dans l'identifiant
                $entrée_saisi = $_POST['entrée_c'];
                $plat_saisi = $_POST['plat_c'];
                $dessert_saisi = $_POST['dessert_c'];
                $etat_saisi  = $_POST['etat'];
                $date_saisi = $_POST['date'];




                // requete SQL pour modifier l'identifiant et/ou le role 
                $update = $bdd -> prepare('UPDATE commander SET entrée_c = ?, plat_c = ?, dessert_c = ?, etat = ?, date = ? WHERE id_commande = ?');
                $update -> execute(array($entrée_saisi, $plat_saisi, $dessert_saisi, $etat_saisi, $date_saisi, $getid));

                echo "Modification des données réussis";
                if(isset($_SESSION['identifiant']) && ($_SESSION['role']==1)){
                    header( 'Location: page_administrateur.php');
                }
                else{
                header( 'Location: client_voir.php');
                }
            }

        }
        else{
            echo "Aucune commande n'a été trouvé";
        }
    }
    else{
        echo "L'id de la commande n'a pas été récupéré";
    }
?>

<!DOCTYPE html>
<html>

    <head>
        <?php 
            include 'header.php';
        ?>
        <link rel="stylesheet" href="style_client.css" type="text/css" media="screen" charset="utf-8">
    </head>

    <body>
        <?php include 'menu.php';?>


        <?php
      
            $mysqli = new mysqli('localhost','grp_6_7','BP3fo7zi9DEq','bdd_6_7');
            $mysqli->set_charset("utf8");
            global $mysqli;


            $requete = "SELECT * FROM `plat` WHERE `catégorie` = 'entrée'";
            $resultat = $mysqli->query($requete);

		?>

        <form method="post" action="">
            <label for="commande_plat"> Formulaire pour commander un plat</label><br><br>
                <br>
                <label for="commande_plat"> Entrée</label>
                <select name="entrée_c" id="entrée">
                    <option value="<?= $entrée ?>"><?= $entrée ?></option>
                    <?php 
                        while ($ligne = $resultat->fetch_assoc()) {
                            ?> 
                            
                            <option value='<?= $ligne["nom"] ?>'><?= $ligne["nom"]. ' ' .$a=$ligne["prix"]. ' '.'€' ?></option>

                            <?php 
                        }
                    ?>
                </select><br>

                <?php

                    $requete = "SELECT * FROM `plat` WHERE `catégorie` = 'plat'";
                    $resultat = $mysqli->query($requete);

                ?>

                <label for="commande_plat"> Plat</label>
                <select name="plat_c" id="plat">
                    <option value="<?= $plat ?>"><?= $plat ?></option>
                     <?php 
                     while ($ligne = $resultat->fetch_assoc()) {
                        ?> 
                        
                        <option value='<?=$ligne["nom"] ?>'><?= $ligne["nom"]. ' ' .$b=$ligne["prix"]. ' '.'€' ?></option>

                        <?php 
                    }
                ?>
                </select><br>

                <?php

                    $requete = "SELECT * FROM `plat` WHERE `catégorie` = 'dessert'";
                    $resultat = $mysqli->query($requete);

                ?>

                <label for="commande_plat"> Dessert</label>
                <select name="dessert_c" id="dessert">
                    <option value="<?= $dessert ?>"><?= $dessert ?></option>
                     <?php 
                     while ($ligne = $resultat->fetch_assoc()) {
                        ?> 
                        
                        <option value='<?=$ligne["nom"] ?>'><?= $ligne["nom"]. ' ' .$c=$ligne["prix"]. ' '.'€'?></option>

                        <?php 
                    }
                ?>
                </select><br>
                <input type="date" id="date" name="date" value="<?=$date ?>"  min="2023-01-01" max="2023-12-31">
                <br>
                
                <?php 
                    echo "Prix total : ". $total=$a+$b+$c;
                    echo "€";

                    //si c'est un administrateur il peut modifier l'état de la commande
                    if(isset($_SESSION['identifiant']) && ($_SESSION['role']==1)){
                        ?>
                        <br>
                        <label for="commande_plat"> Etat de la commande</label>
                            <select name="etat" id="etat">
                                <option value="<?=$etat ?>"><?=$etat ?></option>
                                <option value="en attente">en attente</option>
                                <option value="en préparation">en préparation</option>
                                <option value="prête">prête</option>
                            </select><br>

                        <?php
                    }
                         
                ?>

                <input type="submit" name="modifier_c" value="modifier"/> 
        </form>
        



    </body>

    <?php
        include 'footer.php'; 
    ?>

</html>