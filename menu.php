<?php session_start() // retenir l'identifiant de celui qui utilise le site?> 
<!-- création d'une barre de navigation -->
<nav class="navbar navbar-expand-md navbar-dark bg-dark">
  <div class="container-fluid">

    <a class="navbar-brand" href="#">Esigelec</a>

    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarText">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="index.php">Accueil</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="plats.php">Plats</a>
        </li>
        <li>
          <?php 
              if(isset($_SESSION['identifiant']) && ($_SESSION['role']==0)){ //si c'est un client il peut revenir sur la page client
                echo '<a class="nav-link" href="Connexion_client.php">Client</a>';
              }
              if(isset($_SESSION['identifiant']) && ($_SESSION['role']==1)){ //si c'est un administrateur il peut revenir sur la page administrateur
                echo '<a class="nav-link" href="page_administrateur.php">Administrateur</a>';
              }
          ?>
        </li>
      </ul>
      <ul class="navbar-nav mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link" aria-current="page" href="inscription.php">Créer un compte</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" aria-current="page" href="Page_connexion_client.php">Se connecter</a>
        </li>
        <li class="nav-item">
          <?php 
            if(isset($_SESSION['identifiant'])){ //si l'utilisateur s'est identifié il peut se déconnecter
              echo '<a class="nav-link" href="deconnexion.php">Deconnexion</a>';
            }
            else //si l'utilisateur ne s'est pas enregistrer et connecté il peut le faire
              echo '<a class="nav-link" href="Page_connexion_client.php">Connexion</a>';
          ?>
        </li>
      </ul>
    </div>
  </div>
</nav>