<?php
    session_start();

    if(isset($_POST['login'])){
        extract($_POST);
        // connexion à la base de données
        $db_username = 'grp_6_7';
        $db_password = 'BP3fo7zi9DEq';
        $db_name = 'bdd_6_7';
        $db_host = 'localhost';
        //$db = mysqli_connect($db_host, $db_username, $db_password,$db_name) or die('could not connect to database');

        $db = new mysqli($db_host, $db_username, $db_password, $db_name);
        if ($db->connect_error) {
            die('Erreur de connexion (' . $db->connect_error . ') ' . $db->connect_error);
        }
        
        // on applique les deux fonctions mysqli_real_escape_string et htmlspecialchars
        // pour éliminer toute attaque de type injection SQL et XSS
        $identifiant = mysqli_real_escape_string($db,htmlspecialchars($_POST['identifiant'])); 
        $mot_de_passe = mysqli_real_escape_string($db,htmlspecialchars($_POST['mot_de_passe']));
    

        if(!empty($identifiant) && !empty($mot_de_passe)){ //vérifie que les champs ne sont pas vides
            $q = $db -> prepare("SELECT * FROM client WHERE identifiant = ? "); //inssert une commande SQL pour trouver l'identifiant
            $q -> bind_param("s", $identifiant);
           

            if($q->execute()) {
                /* instead of bind_result: */
                $result = $q->get_result();

        
                    /* fetch the results into an array */
                    while ($myrow = $result->fetch_assoc()) {
            
                        // utiliser $myrow array comme tableau
                        //print_r($myrow);
                        //echo $mot_de_passe;
                        if(password_verify($mot_de_passe,$myrow['mot_de_passe'])){
                            $_SESSION['identifiant']=$_POST['identifiant']; //garde en mémoire l'identifiant dans des cookies
                            $_SESSION['role']=$myrow['role']; //garde en mémoire le mdp dans des cookies
                            $_SESSION['id_client']=$myrow['id_client'];
                            if($myrow['role']==1){
                                echo"<h1> Connexion administrateur réussi </h1>";
                                header('Location: page_administrateur.php');
                                
                            }
                            else{                       
                                echo "<h1> Connexion reussi </h1>";
                                header('Location: Connexion_client.php');

                                
                            }
                            
                        }
                        else {
                            
                            echo "Mot de passe incorrecte ";
                            echo"<a href='Page_connexion_client.php'> Redirection page de connexion </a>";
            
                        }
            
                    }
                
            }
            else {
                header('Location: Page_connexion_client.php');
                echo("erreur lors de l'éxécution");
            }

        
        }
        
        echo("l'identifiant n'existe pas");
        echo"<a href='Page_connexion_client.php'> Redirection page de connexion </a>";
        
        
    }
    
?>

