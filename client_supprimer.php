<?php 
    session_start();
    // on se connecte à la bdd
    include "conf_inscription.php";
    // on vérifie que l'id_commande existe
    if(isset($_GET['id_commande']) AND !empty($_GET['id_commande'])){
        $getid = $_GET['id_commande'];
        $recupCom = $bdd->prepare('SELECT * FROM commander WHERE id_commande = ?');
        $recupCom -> execute(array($getid));
        if($recupCom -> rowCount() > 0){
            $deleteCom = $bdd->prepare('DELETE FROM commander WHERE id_commande= ?');
            $deleteCom -> execute(array($getid));

            header('Location: client_voir.php');
        }
        else{
            echo "Aucune commande n'a été trouvé";
        }
    }
    else{
        echo "La commande n'a pas été récupéré";
    }
?>