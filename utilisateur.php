<!DOCTYPE html>
<!-- affiche tous les utilisateurs pour l'administrateur. Il peut modifier ou supprimer des utilisateurs -->
<html>
    <head>
        <?php
            include 'header.php';
        ?>
        <link rel="stylesheet" href="style_administrateur.css" media="screen" type="text/css" charset="utf-8" />
    </head>

    <body>
        <div id="content">
            <!-- tester si l'utilisateur est connecté -->
            <?php
                include 'menu.php';
                    
                if(!isset($_SESSION['identifiant']) || $_SESSION['role']!=1){
                    header('Location: page_administrateur.php');
                }
                else{                   
                // afficher un message
                echo "Bonjour " .$_SESSION['identifiant']. ", vous êtes connecté en tant que ".$_SESSION['role'] ;
                }
                
            ?>    
            <br>
            
            <form>
                <br>
                <button type="submit" formaction="inscription.php">Ajouter utilisateur</button>
            </form>

            <br>

            <!-- liste des utilisateurs -->
            <table style="caption-side: top;border: 2px solid skyblue; border-collapse: collapse;padding: 8px;">
                <caption style="border: 2px solid skyblue;border-collapse: collapse;padding: 8px;">Liste des utilisateurs</caption>
                <tr style="border: 2px solid skyblue;border-collapse: collapse;padding: 8px;"> <th>Identifiant</th>   <th style="border: 2px solid skyblue;border-collapse: collapse;padding: 8px;">Role</th>  </tr>
                <?php
                    include "conf_inscription.php";
                    $recupUsers = $bdd->query('SELECT * FROM client');
                    while($user = $recupUsers->fetch()){
                        ?>
                        <tr style="border: 2px solid skyblue;border-collapse: collapse;"> <p> <th><?= $user['identifiant']; ?></th>   <th style="border: 2px solid skyblue;border-collapse: collapse;padding: 8px;"><?= $user['role']; ?></th>   

                        <th style="border: 2px solid skyblue;border-collapse: collapse;padding: 8px;"><a href="adm_supprimer.php?id_client=<?= $user['id_client']; ?>" style="color:
                        red; text-decoration: none;">   Supprimer l'utilisateur</a></th> 
                        <th><a href="adm_modifier.php?id_client=<?= $user['id_client']; ?>" style="color:
                        green; text-decoration: none;">   Modifier l'utilisateur</a></th> </p> </tr>                        
                        <?php
                    }
                        
                ?>
            </table>

        </div>
        
    </body>
    <?php
        include 'footer.php'; 
    ?>
</html>