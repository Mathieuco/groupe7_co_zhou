<!DOCTYPE html>

<html>
    <head>
        <?php
            include 'header.php';
        ?>
        <link rel="stylesheet" href="style_administrateur.css" media="screen" type="text/css" />
    </head>

    <body style='background:#fff;'>
        <div id="content">
            <!-- tester si l'utilisateur est connecté -->
            <?php
                include 'menu.php';
                    
                if(!isset($_SESSION['identifiant']) || $_SESSION['role']!=1){
                    header('Location: page_administrateur.php');
                }               
            ?>    
            <!-- formulaire pour ajouter un plat -->
            <form class="box" action="" method="post">
                <h1 class="box-logo box-title"></h1>

                <h1 class="box-title">Veuillez ajoutez un plat</h1>

                    <input type="text" class="box-input" name="nom" placeholder="Nom du plat" required />
                    <br>
                
                    <input type="number" class="box-input" name="prix" placeholder="Prix" required />
                    <br>

                    <input type="text" class="box-input" name="image" placeholder="Lien de l'image" required />
                    <br>

                    <textarea type="text" class="box-input" name="description" placeholder="Description"></textarea>
                    <br>

                    <select name="catégorie" id="catégorie">
                        <option value="">Catégorie du plat</option>
                        <option value="entrée">Entrée</option>
                        <option value="plat">Plat</option>
                        <option value="dessert">Dessert</option>
                    </select>
                    <br>
                
                    <input type="submit" name="ajouter" value="Ajouter" class="box-button" />               
            </form>
            <!-- requête SQL pour ajouter un plat -->
            <?php
                require('conf_inscription.php');
                if(isset($_POST['ajouter'])){

                    extract($_POST);

                    if(!empty($nom) && !empty($prix) && !empty($image) && !empty($catégorie)){

                            global $bdd;                        

                            $q = $bdd->prepare("INSERT INTO plat(nom, prix, image, description, catégorie) VALUES(?,?,?,?,?)");
                            
                            if($q->execute(array($nom,$prix,$image,$description,$catégorie))){
                                $_SESSION['message'] = "Ajout d'un plat réussi";
                                echo $_SESSION['message']; 
                            }
                            else{
                                $_SESSION['message'] = "impossible d'ajouter le plat'";
                                echo $_SESSION['message'];
                            }
                        
                    }
                    else{
                        echo "les champs ne sont pas remplies";
                    }
                }
        ?>
            
        </div>
        
    </body>
    <?php
        include 'footer.php'; 
    ?>
</html>