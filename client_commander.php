<!DOCTYPE html>
<html>

    <head>
        <?php 
            include 'header.php';
        ?>
        <link rel="stylesheet" href="style_client.css" type="text/css" media="screen" charset="utf-8">
    </head>

    <body>
        <?php include 'menu.php';?>


        <?php
      
            $mysqli = new mysqli('localhost','grp_6_7','BP3fo7zi9DEq','bdd_6_7');
            $mysqli->set_charset("utf8");
            global $mysqli;


            $requete = "SELECT * FROM `plat` WHERE `catégorie` = 'entrée'";
            $resultat = $mysqli->query($requete);

		?>

        <form method="post" action="">
            <label for="commande_plat"> Formulaire pour commander un plat</label><br><br>
                <br>
                <label for="commande_plat"> Entrée</label>
                <select name="entrée" id="entrée">
                    <option value="">Faire votre choix</option>
                    <?php 
                        while ($ligne = $resultat->fetch_assoc()) {
                            ?> 
                            
                            <option value='<?= $ligne["nom"] ?>'><?= $ligne["nom"]. ' ' .$a=$ligne["prix"]. ' '.'€' ?></option>

                            <?php 
                        }
                    ?>
                </select><br>

                <?php

                    $requete = "SELECT * FROM `plat` WHERE `catégorie` = 'plat'";
                    $resultat = $mysqli->query($requete);

                ?>

                <label for="commande_plat"> Plat</label>
                <select name="plat" id="plat">
                    <option value="">Faire votre choix</option>
                     <?php 
                     while ($ligne = $resultat->fetch_assoc()) {
                        ?> 
                        
                        <option value='<?=$ligne["nom"] ?>'><?= $ligne["nom"]. ' ' .$b=$ligne["prix"]. ' '.'€' ?></option>

                        <?php 
                    }
                ?>
                </select><br>

                <?php

                    $requete = "SELECT * FROM `plat` WHERE `catégorie` = 'dessert'";
                    $resultat = $mysqli->query($requete);

                ?>

                <label for="commande_plat"> Dessert</label>
                <select name="dessert" id="dessert">
                    <option value="">Faire votre choix</option>
                     <?php 
                     while ($ligne = $resultat->fetch_assoc()) {
                        ?> 
                        
                        <option value='<?=$ligne["nom"] ?>'><?= $ligne["nom"]. ' ' .$c=$ligne["prix"]. ' '.'€'?></option>

                        <?php 
                    }
                ?>
                </select><br>
                <input type="date" id="date" name="date" value="2023-07-22"  min="2023-01-01" max="2023-12-31">
                <br>
                <?php 
                    echo "Prix total : ". $total=$a+$b+$c;
                    echo "€";
                ?>
                
                <input type="submit" name="commander" value="commander"/> 
        </form>
        
        <?php
                require('conf_inscription.php');
                if(isset($_POST['commander'])){

                    extract($_POST);

                            global $bdd;
                            $id=$_SESSION['identifiant'];
                            $getid = $bdd->query("SELECT id_client FROM client WHERE identifiant = '$id' ");                           
                            $id_saisi = $getid->fetch();
                            $_SESSION['id_client']=$id_saisi['id_client'];                    

                            $q = $bdd->prepare("INSERT INTO commander(entrée_c, plat_c, dessert_c, prix_c, etat, id_client, date) VALUES(?,?,?,?,?,?,?)");                           
                                                      
                            if($q->execute(array($entrée,$plat,$dessert,$total,'en attente',$id_saisi['id_client'],$date))){
                                $_SESSION['message'] = "Ajout de la commande réussi";
                                echo $_SESSION['message']; 
                            }
                            else{
                                $_SESSION['message'] = "impossible d'ajouter la commande'";
                                echo $_SESSION['message'];
                            }
                        
                    
                }
        ?>


    </body>

    <?php
        include 'footer.php'; 
    ?>

</html>