<!doctype html>

<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap demo</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <link rel="stylesheet" href="style_client.css" type="text/css" media="screen" charset="utf-8">
  </head>

  <body bgproperties="fixed" background="https://www.normandie.fr/sites/default/files/2022-05/220506-NicolasBroquedis-0177.jpg">

    <?php include 'menu.php'; //mettre une barre de navigation?> 

    <div id="container">
      <p>Bienvenue au restaurant de ESIGELEC !<br>
        Pour réduire le temps d'attente ce site vous offre la posssibilité de voir les plats de la semaine et de passer commande.<br>
        Le personnel du restaurant, vous propose des plats à emporter et de recevoir directement vos commandes.<br>
        Pour accéder à ses services, il faut vous identifier et devenir client. Vouus pouvez le faire frâce aux icones en haut à droite de l'écran.<br>
      </p>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous"></script>
  </body>
  <?php
        include 'footer.php'; 
    ?>
</html>
<!-- page d'accueil-->