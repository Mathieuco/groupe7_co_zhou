<?php
	session_start();
	require 'db/db.php';

	$err = '';
	if($_SERVER['REQUEST_METHOD'] == 'POST'){
		$account = '';
		$password = '';
		if(isset($_POST['account'])){
			$account = $_POST['account'];
		}
		if(isset($_POST['password'])){
			$password = $_POST['password'];
		}
		
		if(empty($account)){
			$err = 'Please Enter Account';
		}else if(empty($password)){
			$err = 'Please Enter Password';
		}else{
			//query
			$db = Db::getInstance();
			$res = $db->getData('sys_2022_11_member',['member_id', 'account', 'password', 'type'],"where account = '".$account."' and type = 1",true);
			if($res){
				$err = 'Exits User';
				
			}else{
				
				$data['account'] = $account;
				$data['password'] = $password;
				$data['type'] = 1;
				$db->insertData('sys_2022_11_member',$data);
				//success
				header('location:/login.php');
				exit();
			}
		}
	}else{
		$db = Db::getInstance();
	}
?>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8" />
		<title>Login</title>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<link rel="stylesheet" href="static/css/bootstrap.min.css" />
		<script type="text/javascript" src="static/js/jquery-2.1.0.min.js"></script>
		<script type="text/javascript" src="static/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="static/css/base.css" />
	</head>

	<body>
		<div class="container">
			<!-- Static navbar -->
			<nav class="navbar navbar-default">
				<div class="container-fluid">
					<div class="navbar-header">
						<a class="navbar-brand" href="index.php">Restaurant</a>
					</div>
					<div id="navbar" class="navbar-collapse collapse">
						<ul class="nav navbar-nav">
							<li class="active">
								<a href="index.php">Home</a>
							</li>
							<li>
								<a href="order.php">Order</a>
							</li>
							<li>
								<a href="admin/login.php">Manager</a>
							</li>
						</ul>
					</div>
					<!--/.nav-collapse -->
				</div>
				<!--/.container-fluid -->
			</nav>
			<div class="row row-offcanvas row-offcanvas-right">
				<div class="col-xs-12 col-sm-12">
					<div class="form row">
						<form class="form-horizontal col-sm-offset-2 col-md-offset-2" action="signin.php" method="post">
							<h3 class="form-title">Sign to your account</h3>
							<div class="col-sm-8 col-md-8">
								<div class="form-group">
									<i class="fa fa-user fa-lg"></i>
									<input class="form-control required" type="text" placeholder="Account" name="account" autofocus="autofocus" />
								</div>
								<div class="form-group">
									<i class="fa fa-lock fa-lg"></i>
									<input class="form-control required" type="password" placeholder="password" name="password" />
								</div>
								<div class="form-group">
									<span style="color:#ff4444;">
									<?php
										echo($err);
									?>
									</span>
								</div>
								<div class="form-group">
                  <a href="/login.php" class="btn btn-success pull-right">Login In</a>
									<input type="submit" class="btn btn-info pull-left" value="Sign Up" />
								</div>
							</div>
						</form>
					</div>
				</div>
				<!--/.col-xs-12.col-sm-9-->
			</div>
			<!--/row-->

		</div>
		<!--/.container-->
	</body>

</html>