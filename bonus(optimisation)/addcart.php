<?php
  session_start();
  require 'db/db.php';
  if(!isset($_SESSION['user'])){
		header('location:/login.php');
		exit();
	}
  $memberid = $_SESSION['user']['member_id'];
  $id = -1;
  if(isset($_GET['id'])){
    $id = $_GET['id'];
    //remove index
    $db = Db::getInstance();
    
    $res = $db->getData('sys_2022_11_cart',['cart_id','member_id','product_id','count'],"where product_id = ".$id." and member_id = ".$memberid,true);
    if($res){
      //update
      $data = [];
      $data['count'] = $res['count'] + 1;
      $db->updateData('sys_2022_11_cart',$data,"where cart_id = ".$res['cart_id']);
    }else{
      $data = [];
      $data['member_id'] = $memberid;
      $data['product_id'] = $id;
      $data['count'] = 1;
      $db->insertData('sys_2022_11_cart',$data);
    }
    header('location:/index.php');
		exit();
  }else{
    header('location:/index.php');
		exit();
  }
?>