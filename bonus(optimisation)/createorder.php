<?php
  session_start();
  require 'db/db.php';
  if(!isset($_SESSION['user'])){
		header('location:login.php');
		exit();
	}
  if(!isset($_GET['order'])){
    header('location:cart.php');
		exit();
  }
  $order = $_GET['order'];
  $arr = explode(',',$order);
  $list = [];
  foreach($arr as $key=>$val)
  {
    $info = explode('=',$val);
    if(count($info) >= 2){
      $data = [];
      $data['id'] = (int)$info[0];
      $data['count'] = (int)$info[1];
      array_push($list,$data);
    }
  }
  $db = Db::getInstance();
  $cart = $db->getList('select a.*,b.product_name,b.moneys from sys_2022_11_cart a left join sys_2022_11_product b on a.product_id = b.product_id where a.member_id = '.$_SESSION['user']['member_id']);
  $orderdetail = [];
  $moneys = 0;
  foreach($cart as $key=>&$val)
  {
    foreach($list as $key1=>$val1)
    {
      if($val1['id'] == $val['cart_id']){
        $val['count'] = $val1['count'];
      }
    }
    //add order
    $detail = [];
    $detail['product_id'] = $val['product_id'];
    $detail['product_name'] = $val['product_name'];
    $detail['product_name'] = $val['product_name'];
    $detail['product_count'] = $val['count'];
    $detail['product_price'] = $val['moneys'];
    $moneys += $val['count'] * $val['moneys'];
    array_push($orderdetail,$detail);

    //delete cart
    $res = $db->deleteData('sys_2022_11_cart',"where cart_id = ".$val['cart_id']);
  }

  $orderdata = [];
  $ordercode = date('Ymd') . str_pad(mt_rand(1, 99999), 5, '0', STR_PAD_LEFT);
  $orderdata['order_code'] = $ordercode;
  $orderdata['member_id'] = $_SESSION['user']['member_id'];
  $orderdata['status'] = 0;
  $orderdata['pay_moneys'] = $moneys;
  $res = $db->insertData('sys_2022_11_order',$orderdata);
  if($res){
    foreach($orderdetail as $key=>&$val)
    {
      $val['order_id'] = $res;
      $db->insertData('sys_2022_11_order_detail',$val);
    }
    header('location:order.php');
  }else{
    header('location:cart.php');
  }
?>