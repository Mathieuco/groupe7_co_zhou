<?php
  session_start();
	require 'db/db.php';
  if(!isset($_SESSION['user'])){
    header('location:/login.php');
    exit();
  }

	//query product
	$db = Db::getInstance();

	//query user
	$cartNumber = 0;
	$cartMoneys = 0;
  $islogin = true;
  $name = $_SESSION['user']['account'];
  $cart = $db->getList('select a.*,b.product_name,b.moneys from sys_2022_11_cart a left join sys_2022_11_product b on a.product_id = b.product_id where a.member_id = '.$_SESSION['user']['member_id']);
  foreach($cart as $key=>$val)
  {
    $cartNumber += $val['count'];
    $cartMoneys += $val['count'] * $val['moneys'];
  }
?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>Index</title>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<link rel="stylesheet" href="static/css/bootstrap.min.css" />
		<script type="text/javascript" src="static/js/jquery-2.1.0.min.js"></script>
		<script type="text/javascript" src="static/js/bootstrap.min.js"></script>
		<style>
			.order{
				border-bottom: 1px solid #eee;
				margin-bottom: 20px;
			}
			.product-grid{
				overflow: hidden;
				width: 100%;
				padding: 10px;
				box-sizing: border-box;
			}
			.product-grid .image{
				width: ;
			}
			.price {
				overflow: hidden;
				width: 100%;
			}
			.price .fl{
				float: left;
			}
			.price .fr{
				float: right;
			}
		</style>
		<link rel="stylesheet" href="static/css/base.css" />
	</head>

	<body>
		<div class="container">
			<!-- Static navbar -->
			<nav class="navbar navbar-default">
				<div class="container-fluid">
					<div class="navbar-header">
						<a class="navbar-brand" href="index.php">Restaurant</a>
					</div>
					<div id="navbar" class="navbar-collapse collapse">
						<ul class="nav navbar-nav">
							<li>
								<a href="index.php">Home</a>
							</li>
							<li class="active">
								<a href="order.php">Order</a>
							</li>
							<li>
								<a href="admin/login.php">Manager</a>
							</li>
						</ul>
						<?php
							if(!$islogin){
						?>
						<ul class="nav navbar-nav navbar-right">
							<li class="active">
								<a href="login.php">Login</a>
							</li>
							<li>
								<a href="signin.php">Sign Up</a>
							</li>
						</ul>
						<?php
							}else{
						?>
							<ul class="nav navbar-nav navbar-right">
								<li>
									<a href="loginout.php">Login Out</a>
								</li>
							</ul>
						<?php
							}
						?>
					</div>
					<!--/.nav-collapse -->
				</div>
				<!--/.container-fluid -->
			</nav>
			<div class="row row-offcanvas row-offcanvas-right">
				<div class="site-branding-area">
					<div class="container">
						<div class="row">
							<div class="col-sm-6">
								<div class="logo">
									<h1>Hello<?php echo($name ? ','.$name : '') ?></h1>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="shopping-item">
									<a href="cart.php">Cart - <span class="cart-amunt">$
									<?php echo($cartMoneys) ?>
									</span><span class="product-count"><?php echo($cartNumber) ?></span></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row row-offcanvas row-offcanvas-right">
				<div class="col-xs-12 col-sm-12">
					<!-- 右9 -->
					<table class="table table-hover" id="table">
						<thead>
							<tr class="bg-primary">
								<th scope="col">Product Name</th>
								<th scope="col">Moneys</th>
								<th scope="col">Buy Count</th>
								<th scope="col">Opt</th>
							</tr>
						</thead>
						<tbody>
              <?php
                foreach($cart as $key=>$val)
                {
              ?>
							<tr class="line">
								<th scope="row">
                  <?php echo($val['product_name']); ?>
                </th>
								<td>$<?php echo($val['moneys']); ?></td>
								<td>
                  <input class="count" type="text" value="<?php echo($val['count']); ?>">
                  <input class="id" type="hidden" value="<?php echo($val['cart_id']); ?>">
                </td>
								<td>
									<a href="<?php echo("deletecart.php?id=".$val['cart_id']); ?>">删除</td>
							</tr>
              <?php
                }
              ?>
						</tbody>
					</table>
					<p class="yyy">
						<a href="cleancart.php" class="btn btn-danger">Clean Card</a>
						<a class="btn btn-outline-success" href="index.php">Go Home</a>
						<button class="btn btn-primary" data-toggle="modal" id="create">Create Order</button>
					</p>
				</div>
				<!--/.col-xs-12.col-sm-9-->
			</div>
			<!--/row-->
		</div>
		<!--/.container-->
		<script>
			$("#create").click(function(){
				var cart = $("#table .line");
				var order = '';
				cart.each(function(){
					var id = $(this).find('.id').val();
					var count = $(this).find('.count').val();
					if(order){
						order += "," + id + '=' + count;
					}else{
						order += id + '=' + count;
					}
				});
				window.location.href = 'createorder.php?order='+order;
			});
		</script>
	</body>
</html>