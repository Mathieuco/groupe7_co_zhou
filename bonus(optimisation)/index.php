<?php
  session_start();
	require 'db/db.php';

	//query product
	$db = Db::getInstance();
	$list = $db->getData('sys_2022_11_product',['product_id', 'product_name', 'spec', 'product_cover', 'product_detail','moneys','classify']," order by product_id desc");

	//query user
	$cartNumber = 0;
	$cartMoneys = 0;
	$name = '';
	$islogin = false;
	if(isset($_SESSION['user'])){
		$islogin = true;
		$name = $_SESSION['user']['account'];
		$cart = $db->getList('select a.*,b.moneys from sys_2022_11_cart a left join sys_2022_11_product b on a.product_id = b.product_id where a.member_id = '.$_SESSION['user']['member_id']);
		foreach($cart as $key=>$val)
		{
			$cartNumber += $val['count'];
			$cartMoneys += $val['count'] * $val['moneys'];
		}
	}
?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>Index</title>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<link rel="stylesheet" href="static/css/bootstrap.min.css" />
		<script type="text/javascript" src="static/js/jquery-2.1.0.min.js"></script>
		<script type="text/javascript" src="static/js/bootstrap.min.js"></script>
		<style>
			.product-grid {
				font-family: 'Open Sans', sans-serif;
				text-align: center;
			}
			
			.product-grid .product-image {
				position: relative;
				overflow: hidden;
			}
			
			.product-grid .product-image a.image {
				display: block;
			}
			
			.product-grid .product-image img {
				width: 100%;
				height: 200px;
				transition: all 0.3s;
			}
			
			.product-grid .product-image:hover img {
				transform: scale(1.05);
			}
			
			.product-grid .product-content {
				width: 100%;
				padding: 12px 0;
				display: inline-block;
			}
			
			.product-grid .title {
				margin: 0 0 7px;
				font-size: 16px;
				font-weight: 600;
				text-transform: capitalize;
			}
			
			.product-grid .title a {
				color: #000;
				transition: all 0.4s ease-out;
			}
			
			.product-grid .title a:hover {
				color: #d9534f;
			}
			
			.product-grid .price {
				color: #000;
				font-size: 16px;
				font-weight: 600;
				width: calc(100% - 100px);
				margin: 0 0 10px;
				display: inline-block;
			}
			
			.product-grid .price span {
				color: #7a7a7a;
				font-size: 15px;
				text-decoration: line-through;
				margin-right: 5px;
				display: inline-block;
			}
			
			.product-grid .rating {
				padding: 0;
				margin: 0;
				list-style: none;
				display: inline-block;
				float: right;
			}
			
			.product-grid .rating li {
				color: #ffc500;
				font-size: 13px;
			}
			
			.product-grid .rating li.far {
				color: #bababa;
			}
			
			.product-grid .add-to-cart {
				color: #000;
				background: #fff;
				font-size: 13px;
				font-weight: 600;
				text-align: left;
				width: 75%;
				margin: 0 auto;
				border: 1px solid #d9534f;
				display: block;
				transition: all .3s ease;
			}
			
			.product-grid .add-to-cart:hover {
				color: #fff;
				background: #d9534f;
			}
			
			.product-grid .add-to-cart i {
				color: #fff;
				background-color: #d9534f;
				text-align: center;
				line-height: 35px;
				height: 35px;
				width: 35px;
				border-right: 1px solid #fff;
				display: inline-block;
			}
			
			.product-grid .add-to-cart span {
				text-align: center;
				line-height: 35px;
				height: 35px;
				width: calc(100% - 40px);
				padding: 0 6px;
				vertical-align: top;
				display: inline-block;
			}
			
			@media only screen and (max-width:990px) {
				.product-grid {
					margin: 0 0 30px;
				}
			}
		</style>
		<link rel="stylesheet" href="static/css/base.css" />
	</head>

	<body>
		<div class="container">
			<!-- Static navbar -->
			<nav class="navbar navbar-default">
				<div class="container-fluid">
					<div class="navbar-header">
						<a class="navbar-brand" href="index.php">Restaurant</a>
					</div>
					<div id="navbar" class="navbar-collapse collapse">
						<ul class="nav navbar-nav">
							<li class="active">
								<a href="index.php">Home</a>
							</li>
							<li>
								<a href="order.php">Order</a>
							</li>
							<li>
								<a href="admin/login.php">Manager</a>
							</li>
						</ul>
						<?php
							if(!$islogin){
						?>
						<ul class="nav navbar-nav navbar-right">
							<li class="active">
								<a href="login.php">Login</a>
							</li>
							<li>
								<a href="signin.php">Sign Up</a>
							</li>
						</ul>
						<?php
							}else{
						?>
							<ul class="nav navbar-nav navbar-right">
								<li>
									<a href="loginout.php">Login Out</a>
								</li>
							</ul>
						<?php
							}
						?>
					</div>
					<!--/.nav-collapse -->
				</div>
				<!--/.container-fluid -->
			</nav>
			<div class="row row-offcanvas row-offcanvas-right">
				<div class="site-branding-area">
					<div class="container">
						<div class="row">
							<div class="col-sm-6">
								<div class="logo">
									<h1>Hello<?php echo($name ? ','.$name : '') ?></h1>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="shopping-item">
									<a href="cart.php">Cart - <span class="cart-amunt">$
									<?php echo($cartMoneys) ?>
									</span><span class="product-count"><?php echo($cartNumber) ?></span></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row row-offcanvas row-offcanvas-right">
				<div class="col-xs-12 col-sm-12">
					<div class="jumbotron">
						<h1>Welcome!</h1>
						<p>This is a simple restaurant. I hope the menu will meet your needs.</p>
					</div>
					<div class="row">
						<?php
							foreach($list as $key=>$val)
							{
						?>
						<!--/.col-xs-6.col-lg-4-->
						<div class="col-sm-6 col-xs-12 col-lg-4">
							<div class="product-grid">
								<div class="product-image">
									<div class="image"><img class="pic-1" src="<?php echo($val['product_cover'] ? $val['product_cover'] : '/upload/none.png') ?>"></div>
								</div>
								<div class="product-content">
									<h3 class="title"><?php echo($val['product_name']) ?></h3>
									<div class="price">$<?php echo($val['moneys']) ?></div>
									<div class="title"><?php echo($val['classify']) ?></div>
									<a class="add-to-cart" href="<?php echo('addcart.php?id='.$val['product_id']) ?>"><i class="fas fa-shopping-cart"></i><span>ADD TO CART</span></a>
								</div>
							</div>
						</div>
						<!--/.col-xs-6.col-lg-4-->
						<?php
							}
						?>
					</div>
					<!--/row-->
				</div>
				<!--/.col-xs-12.col-sm-9-->
			</div>
			<!--/row-->
		</div>
		<!--/.container-->
	</body>

</html>