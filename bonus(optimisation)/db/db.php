<?php
class Db{
	
	private  static $instance;
 
	private  $pdo;
 
	private $stmt;

	private function __construct($config,$port,$charset){
		try{
			$this->pdo = new  PDO('mysql:host='.$config['host'].';dbname='.$config['dbname'].';port='.$port.';charset='.$charset,$config['user'],$config['password']);
			//$this->pdo ->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
		}catch(PDOException $e){
			header("Content-type: text/html;charset=utf-8"); 
			echo 'mysql connect error：'.$e->getMessage();
		}
	}
	
	public static function getInstance(){
		if(!self::$instance instanceof Db ) {
			$port=3306;
			$charset="utf8";
			$config=['host'=>'127.0.0.1','dbname'=>'sys_test','user'=>'root','password'=>'root'];
			self::$instance = new self($config,$port,$charset);
		}		
		return self::$instance;
	}
	
	public function insertData($table,$data){		
		$columns = implode(',',array_keys($data));

		$values = ':'.implode(',:',array_keys($data));		
		$sql = "INSERT INTO $table ($columns) VALUES($values)";
		$newData = $this->doData($data);
		$this->exe($sql,$newData);
		if($this->stmt->errorCode()==00000) return $this->pdo->lastInsertId();
		else return 0;		
	}
	
	public function updateData($table,$data,$where){
		//$sql = "UPDATE user SET user_name=:name WHERE id=:id";		

		$res = $this->getData($table,array_keys($data),$where);
		if(!$res) return 'error';		
		$columns = '';
		foreach($data as $k=>$v) $columns.=$k.'=:'.$k.',';
		$columns = trim($columns,',');		
		$sql = "UPDATE $table SET $columns ".$where;
		$newData = $this->doData($data);
		$this->exe($sql,$newData);		
		if($this->stmt->errorCode()==00000) return 1;			
		else return 0;
	}
 
	public function getData($table,$fields,$where=null,$one=false){
		if(count($fields)>1) $columns = implode(',',array_values($fields));			
		else $columns = $fields[0];
		$sql = "SELECT $columns FROM $table ".$where;
		$this->exe($sql);
		if($this->stmt->errorCode()==00000){
			if($one) return $this->stmt->fetch(PDO::FETCH_ASSOC);
			else return $this->stmt->fetchAll(PDO::FETCH_ASSOC);			
		}else return [];
	}	

	public function getList($sql,$one=false){
		$this->exe($sql);
		if($this->stmt->errorCode()==00000){
			if($one) return $this->stmt->fetch(PDO::FETCH_ASSOC);
			else return $this->stmt->fetchAll(PDO::FETCH_ASSOC);			
		}else return [];
	}	
 
	public function deleteData($table,$where){
		if(!$where) return 'enter where';
		$sql = "DELETE FROM $table ".$where;
		$res = $this->getData($table,['*'],$where);
		if(!$res) return 0;		
		$this->exe($sql);		  
		if($this->stmt->errorCode()==00000) return 1;			
		else return 0;
	}
	
	private function doData($data){
		foreach($data as $k=>$v){
			$key = ':'.$k;
			$newData[$key]=$v;
		}
		return $newData;
	}
	
	private function exe($sql,$data=null){
		$this->stmt = $this->pdo->prepare($sql);
    $this->stmt->execute($data);
	}
	
	private function __clone(){}	
}
?>