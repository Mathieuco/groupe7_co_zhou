<?php
  session_start();
	require '../db/db.php';
  if(!isset($_SESSION['login'])){
		header('location:/admin/login.php');
		exit();
	}
  $id = -1;
  $err = '';
  $res = [];
  if($_SERVER['REQUEST_METHOD'] == 'POST'){
    //edit
    $id = -1;
    $product_name = '';
    $spec = '';
    $product_cover = '';
    $product_detail = '';
    $moneys = '';
    $classify = '';
    if(isset($_POST['product_id'])){
      $id = $_POST['product_id'];
    }
    if(isset($_POST['product_name'])){
      $product_name = $_POST['product_name'];
    }
    if(isset($_POST['spec'])){
      $spec = $_POST['spec'];
    }
    if(isset($_POST['product_detail'])){
      $product_detail = $_POST['product_detail'];
    }
    if(isset($_POST['moneys'])){
      $moneys = $_POST['moneys'];
    }
    if(isset($_POST['classify'])){
      $classify = $_POST['classify'];
    }
    if(isset($_FILES["file"])){
      //upload image
      if ($_FILES["file"]["error"] > 0)
	    {
	    	$err = 'upload file error';
	    }
	  	else
	    {
        $ext = strrchr($_FILES["file"]["name"],'.');
        $name = time().'';
        move_uploaded_file($_FILES["file"]["tmp_name"],"../upload/" .$name.$ext);
        $product_cover = "/upload/" .$name.$ext;
      }
    }

    if(empty($product_name)){
			$err = 'Please Enter Product Name';
		}else if(empty($spec)){
			$err = 'Please Enter Spec';
		}else if(empty($product_detail)){
			$err = 'Please Enter Product Detail';
		}else if(empty($moneys)){
			$err = 'Please Enter Moneys';
		}else{
			//query
			$db = Db::getInstance();
      $data = [];
      $data['product_name'] = $product_name;
      $data['spec'] = $spec;
      if($product_cover){
        $data['product_cover'] = $product_cover;
      }
      $data['product_detail'] = $product_detail;
      $data['moneys'] = $moneys;
      $data['classify'] = $classify;
      $res = 0;
      if($id > 0){
        $res = $db->updateData('sys_2022_11_product',$data,"where product_id = ".$id);
      }else{
        $res = $db->insertData('sys_2022_11_product',$data);
      }
			if($res){
				header('location:/admin/product.php');
				exit();
			}else{
				$err = 'Save Error';
			}
		}
  }else{
    //query product
    $db = Db::getInstance();
    
    if(isset($_GET['id'])){
      $id = $_GET['id'];
    }
    $res = $db->getData('sys_2022_11_product',['product_id', 'product_name', 'spec', 'product_cover', 'product_detail', 'moneys', 'classify']," where product_id = ".$id,true);
    if(!$res){
      $id = -1;
      $res = [];
      $res['product_name'] = '';
      $res['spec'] = '';
      $res['product_cover'] = '';
      $res['product_detail'] = '';
      $res['moneys'] = '';
      $res['classify'] = '';
    }
  }
?>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8" />
		<title>Product</title>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<link rel="stylesheet" href="../static/css/bootstrap.min.css" />
		<script type="text/javascript" src="../static/js/jquery-2.1.0.min.js"></script>
		<script type="text/javascript" src="../static/js/bootstrap.min.js"></script>
    <style>
			.order {
				border-bottom: 1px solid #eee;
				margin-bottom: 20px;
				padding: 20px;
				box-sizing: border-box;
			}
			
			.product-grid {
				overflow: hidden;
				width: 100%;
				padding: 10px;
				box-sizing: border-box;
			}
			
			.product-grid .image {
				width: ;
			}
			
			.price {
				overflow: hidden;
				width: 100%;
			}
			
			.price .fl {
				float: left;
			}
			
			.price .fr {
				float: right;
			}
		</style>
	</head>

	<body>
		<nav class="navbar navbar-inverse">
			<div class="container-fluid">
				<div class="navbar-header">
					<a class="navbar-brand" href="product.php">Manager</a>
				</div>
				<ul class="nav navbar-nav navbar-right">
						<li class="active">
							<a href="loginout.php">Quitter</a>
						</li>
					</ul>
			</div>
		</nav>

		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-3 col-md-2 sidebar">
					<ul class="nav nav-sidebar">
						<li class="active">
							<a href="product.php">Produit</a>
						</li>
						<li>
							<a href="order.php">Command</a>
						</li>
						<li>
							<a href="user.php">Client</a>
						</li>
					</ul>
				</div>
				<div class="col-sm-9 col-md-10 main">
        <div class="row order">
						<div class="col-sm-12">
							<form action="editproduct.php" method="post" enctype="multipart/form-data">
								<input type="hidden" name="product_id" value="<?php echo($id); ?>">
								<div class="form-group">
									<label>Nom</label>
									<input type="text" class="form-control" value="<?php echo($res['product_name']); ?>" name="product_name">
								</div>
								<div class="form-group">
									<label>Spec</label>
									<input type="text" class="form-control" name="spec" value="<?php echo($res['spec']); ?>">
                  <small class="form-text text-muted">Separated by commas.</small>
								</div>
								<div class="form-group">
									<label>Image</label>
                  <?php if($res['product_cover']) { ?>
                  <img src="<?php echo($res['product_cover']) ?>" />
                  <?php } ?>
									<input type="file" class="form-control" name="file" accept="image/*">
								</div>
								<div class="form-group">
									<label>Detail</label>
                  <input type="text" class="form-control" value="<?php echo($res['product_detail']); ?>" name="product_detail">
								</div>
                <div class="form-group">
									<label>Prix</label>
                  <input type="text" class="form-control" value="<?php echo($res['moneys']); ?>" name="moneys">
								</div>
                <div class="form-group">
									<label>Classify</label>
                  <select class="form-control" name="classify">
                    <option value='entree' <?php echo($res['classify'] == 'entree' ? 'selected="selected"' : ''); ?>>entree</option>
                    <option value='plat' <?php echo($res['classify'] == 'plat' ? 'selected="selected"' : ''); ?>>plat</option>
                    
                    <option value='boisson' <?php echo($res['classify'] == 'drinks' ? 'selected="selected"' : ''); ?>>boisson</option>
                    <option value='dessert' <?php echo($res['classify'] == 'dessert' ? 'selected="selected"' : ''); ?>>dessert</option>
                  </select>
								</div>
                <div class="form-group">
									<span style="color:#ff4444;">
									<?php
										echo($err);
									?>
									</span>
								</div>
								<button type="submit" class="btn btn-primary">Submit</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>