<?php
  session_start();
	require '../db/db.php';
  if(!isset($_SESSION['login'])){
		header('location:/admin/login.php');
		exit();
	}

	//query product
	$db = Db::getInstance();
	$list = $db->getData('sys_2022_11_product',['product_id', 'product_name', 'spec', 'product_cover', 'product_detail', 'moneys', 'classify']," order by product_id desc");
?>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8" />
		<title>Product</title>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<link rel="stylesheet" href="../static/css/bootstrap.min.css" />
		<script type="text/javascript" src="../static/js/jquery-2.1.0.min.js"></script>
		<script type="text/javascript" src="../static/js/bootstrap.min.js"></script>
	</head>

	<body>
		<nav class="navbar navbar-inverse">
			<div class="container-fluid">
				<div class="navbar-header">
					<a class="navbar-brand" href="product.php">Manager</a>
				</div>
				<ul class="nav navbar-nav navbar-right">
						<li class="active">
							<a href="/../index.php">Quitter</a>
						</li>
					</ul>
			</div>
		</nav>

		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-3 col-md-2 sidebar">
					<ul class="nav nav-sidebar">
						<li class="active">
							<a href="product.php">Produit</a>
						</li>
						<li>
							<a href="order.php">Command</a>
						</li>
						<li>
							<a href="user.php">Client</a>
						</li>
					</ul>
				</div>
				<div class="col-sm-9 col-md-10 main">
					<h2 class="sub-header">Produit</h2>
					<div class="table-responsive">
						<div>
							<a href="editproduct.php" class="btn btn-primary">Ajouter</a>
						</div>
						<table class="table table-striped">
							<thead>
								<tr>
									<th>ID</th>
									<th>Nom</th>
									<th>Image</th>
									<th>Spec</th>
									<th>Prix</th>
									<th>Classify</th>
                  <th>Opt</th>
								</tr>
							</thead>
							<tbody>
								<?php
									foreach($list as $key=>$val)
									{
								?>
									<tr>
										<td>
											<?php echo($val['product_id']) ?>
										</td>
										<td>
											<?php echo($val['product_name']) ?>
										</td>
										<td>
											<?php echo($val['product_cover']) ?>
										</td>
										<td>
											<?php echo($val['spec']) ?>
										</td>
										<td>
											<?php echo($val['moneys']) ?>
										</td>
										<td>
											<?php echo($val['classify']) ?>
										</td>
										<td>
											<a class="btn btn-link" href="<?php echo('editproduct.php?id='.$val['product_id']) ?>">Modifier</a>
											<a class="btn btn-link" href="<?php echo('deleteproduct.php?id='.$val['product_id']) ?>">Suprimer</a>
										</td>
									</tr>
								<?php
									}
								?>
								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>