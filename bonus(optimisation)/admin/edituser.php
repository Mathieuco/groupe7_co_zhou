<?php
  session_start();
	require '../db/db.php';
  if(!isset($_SESSION['login'])){
		header('location:/admin/login.php');
		exit();
	}
  $id = -1;
  $err = '';
  $res = [];
  if($_SERVER['REQUEST_METHOD'] == 'POST'){
    //edit
    $id = -1;
    $account = '';
    $password = '';
    $type = 0;
    if(isset($_POST['member_id'])){
      $id = $_POST['member_id'];
    }
    if(isset($_POST['account'])){
      $account = $_POST['account'];
    }
    if(isset($_POST['password'])){
      $password = $_POST['password'];
    }
    if(isset($_POST['type'])){
      $type = $_POST['type'];
    }
    
    if(empty($account)){
			$err = 'Please Enter Account';
		}else if(empty($password)){
			$err = 'Please Enter Password';
		}else{
			//query
			$db = Db::getInstance();
      $data = [];
      $data['account'] = $account;
      $data['password'] = $password;
      $data['type'] = $type;
      $res = 0;
      if($id > 0){
        $res = $db->updateData('sys_2022_11_member',$data,"where member_id = ".$id);
      }else{
        $res = $db->insertData('sys_2022_11_member',$data);
      }
			if($res){
				header('location:/admin/user.php');
				exit();
			}else{
				$err = 'Save Error';
			}
		}
  }else{
    //query product
    $db = Db::getInstance();
    
    if(isset($_GET['id'])){
      $id = $_GET['id'];
    }
    $res = $db->getData('sys_2022_11_member',['member_id', 'account', 'password', 'type']," where member_id = ".$id,true);
    if(!$res){
      $id = -1;
      $res = [];
      $res['account'] = '';
      $res['password'] = '';
      $res['type'] = '';
    }
  }
?>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8" />
		<title>Product</title>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<link rel="stylesheet" href="../static/css/bootstrap.min.css" />
		<script type="text/javascript" src="../static/js/jquery-2.1.0.min.js"></script>
		<script type="text/javascript" src="../static/js/bootstrap.min.js"></script>
    <style>
			.order {
				border-bottom: 1px solid #eee;
				margin-bottom: 20px;
				padding: 20px;
				box-sizing: border-box;
			}
			
			.product-grid {
				overflow: hidden;
				width: 100%;
				padding: 10px;
				box-sizing: border-box;
			}
			
			.product-grid .image {
				width: ;
			}
			
			.price {
				overflow: hidden;
				width: 100%;
			}
			
			.price .fl {
				float: left;
			}
			
			.price .fr {
				float: right;
			}
		</style>
	</head>

	<body>
		<nav class="navbar navbar-inverse">
			<div class="container-fluid">
				<div class="navbar-header">
					<a class="navbar-brand" href="product.php">Manager</a>
				</div>
				<ul class="nav navbar-nav navbar-right">
						<li class="active">
							<a href="loginout.php">Quitter</a>
						</li>
					</ul>
			</div>
		</nav>

		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-3 col-md-2 sidebar">
					<ul class="nav nav-sidebar">
						<li class="active">
							<a href="product.php">Product</a>
						</li>
						<li>
							<a href="order.php">Order</a>
						</li>
						<li>
							<a href="user.php">User</a>
						</li>
					</ul>
				</div>
				<div class="col-sm-9 col-md-10 main">
        <div class="row order">
						<div class="col-sm-12">
							<form action="edituser.php" method="post">
								<input type="hidden" name="member_id" value="<?php echo($id); ?>">
								<div class="form-group">
									<label>Account</label>
									<input type="text" class="form-control" value="<?php echo($res['account']); ?>" name="account">
								</div>
								<div class="form-group">
									<label>Password</label>
									<input type="password" class="form-control" name="password" value="<?php echo($res['password']); ?>">
								</div>
                <div class="form-group">
									<label>Member Type</label>
                  <select class="form-control" name="type">
                    <option value='0' <?php echo($res['type'] == 0 ? 'selected="selected"' : ''); ?>>manager</option>
                    <option value='1' <?php echo($res['type'] == 1 ? 'selected="selected"' : ''); ?>>user</option>
                  </select>
								</div>
                <div class="form-group">
									<span style="color:#ff4444;">
									<?php
										echo($err);
									?>
									</span>
								</div>
								<button type="submit" class="btn btn-primary">Submit</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>