<?php
  session_start();
	require '../db/db.php';
  if(!isset($_SESSION['login'])){
		header('location:/admin/login.php');
		exit();
	}

	//query product
	$db = Db::getInstance();
	$list = $db->getData('sys_2022_11_member',['member_id', 'account', 'password', 'type']," order by member_id desc");
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>User</title>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<link rel="stylesheet" href="../static/css/bootstrap.min.css" />
		<script type="text/javascript" src="../static/js/jquery-2.1.0.min.js"></script>
		<script type="text/javascript" src="../static/js/bootstrap.min.js"></script>
	</head>

	<body>
		<nav class="navbar navbar-inverse">
			<div class="container-fluid">
				<div class="navbar-header">
					<a class="navbar-brand" href="product.php">Manager</a>
				</div>
				<ul class="nav navbar-nav navbar-right">
						<li class="active">
							<a href="loginout.php">Quitter</a>
						</li>
					</ul>
			</div>
		</nav>

		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-3 col-md-2 sidebar">
					<ul class="nav nav-sidebar">
						<li class="active">
							<a href="product.php">Produit</a>
						</li>
						<li>
							<a href="order.php">Command</a>
						</li>
						<li>
							<a href="user.php">Client</a>
						</li>
					</ul>
				</div>
				<div class="col-sm-9 col-md-10 main">
					<h2 class="sub-header">Client</h2>
					<div class="table-responsive">
					<div>
							<a href="edituser.php" class="btn btn-primary">Ajouter Client</a>
						</div>
						<table class="table table-striped">
							<thead>
								<tr>
									<th>ID</th>
									<th>Compte</th>
									<th>Type</th>
									<th>Opt</th>
								</tr>
							</thead>
							<tbody>
							<?php
									foreach($list as $key=>$val)
									{
								?>
									<tr>
										<td>
											<?php echo($val['member_id']) ?>
										</td>
										<td>
											<?php echo($val['account']) ?>
										</td>
										<td>
											<?php echo($val['type'] == 0 ? 'manager' : 'user') ?>
										</td>
										<td>
											<a class="btn btn-link" href="<?php echo('edituser.php?id='.$val['member_id']) ?>">Modifier</a>
											<a class="btn btn-link" href="<?php echo('deleteuser.php?id='.$val['member_id']) ?>">Suprimer</a>
										</td>
									</tr>
								<?php
									}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</body>

</html>