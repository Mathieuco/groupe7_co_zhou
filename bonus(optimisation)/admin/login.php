<?php
	session_start();
	require '../db/db.php';
	$err = '';
	if($_SERVER['REQUEST_METHOD'] == 'POST'){
		$account = '';
		$password = '';
		if(isset($_POST['account'])){
			$account = $_POST['account'];
		}
		if(isset($_POST['password'])){
			$password = $_POST['password'];
		}
		
		if(empty($account)){
			$err = 'Please Enter Account';
		}else if(empty($password)){
			$err = 'Please Enter Password';
		}else{
			//query
			$db = Db::getInstance();
			$res = $db->getData('sys_2022_11_member',['member_id', 'account', 'password', 'type'],"where account = '".$account."' and password= '".$password."' and type = 0",true);
			if($res){
				//success
				$_SESSION['login'] = $res;
				header('location:/admin/product.php');
				exit();
			}else{
				$err = 'Account Or Password Error';
			}
		}
	}else{
		$db = Db::getInstance();
	}
?>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8" />
		<title>manager</title>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<link rel="stylesheet" href="../static/css/bootstrap.min.css" />
		<script type="text/javascript" src="../static/js/jquery-2.1.0.min.js"></script>
		<script type="text/javascript" src="../static/js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="../static/css/base.css" />
	</head>

	<body>
		<div class="container">
			<div class="row row-offcanvas" style="padding-top:120px;">
				<div class="col-xs-12 col-sm-12">
					<div class="form row">
						<form class="form-horizontal col-sm-offset-2 col-md-offset-2" action="login.php" method="post">
							<div class="col-sm-8 col-md-8">
              <h3 class="form-title text-center">Login to your Account</h3>
								<div class="form-group">
									<i class="fa fa-user fa-lg"></i>
									<input class="form-control required" type="text" placeholder="Account" name="account" autofocus="autofocus" />
								</div>
								<div class="form-group">
									<i class="fa fa-lock fa-lg"></i>
									<input class="form-control required" type="password" placeholder="Password" name="password" />
								</div>
								<div class="form-group">
									<span style="color:#ff4444;">
									<?php
										echo($err);
									?>
									</span>
								</div>
								<div class="form-group">
									<input type="submit" class="btn btn-info" style="width:100%;" value="Login" />
								</div>
							</div>
						</form>
					</div>
				</div>
				<!--/.col-xs-12.col-sm-9-->
			</div>
			<!--/row-->
			<hr>
		</div>
		<!--/.container-->
	</body>

</html>