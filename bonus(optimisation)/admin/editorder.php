<?php
  session_start();
	require '../db/db.php';
  if(!isset($_SESSION['login'])){
		header('location:/admin/login.php');
		exit();
	}
  $id = -1;
  $err = '';
  $res = [];
  if($_SERVER['REQUEST_METHOD'] == 'POST'){
    //edit
    $id = -1;
    $status = '';
    if(isset($_POST['order_id'])){
      $id = $_POST['order_id'];
    }
    if(isset($_POST['status'])){
      $status = $_POST['status'];
    }

    if(empty($status)){
			$err = 'Please Select Status';
		}else{
			//query
			$db = Db::getInstance();
      $data = [];
      $data['status'] = $status;
      $res = 0;
      if($id > 0){
        $res = $db->updateData('sys_2022_11_order',$data,"where order_id = ".$id);
      }else{
        $err = 'Not Exits Order';
      }
			if($res){
				header('location:/admin/order.php');
				exit();
			}else{
				$err = 'Save Error';
			}
		}
  }else{
    //query product
    $db = Db::getInstance();
    
    if(isset($_GET['id'])){
      $id = $_GET['id'];
    }
    $orderlist = $db->getData('sys_2022_11_order_detail',['order_detail_id', 'order_id', 'product_id', 'product_name', 'product_cover','product_count','product_price']," where order_id = ".$id);

    $res = $db->getData('sys_2022_11_order',['order_id', 'order_code', 'member_id', 'status', 'pay_moneys','create_time']," where order_id = ".$id,true);
    if(!$res){
      $id = -1;
      $res = [];
      $res['order_id'] = '';
      $res['member_id'] = '';
      $res['status'] = '';
      $res['pay_moneys'] = '';
      $res['member'] = '';
    }else{
      //query member
      $member = $db->getData('sys_2022_11_member',['member_id', 'account', 'password', 'type']," where member_id = ".$res['member_id'],true);
      if($member){
        $res['member'] = $member['account'];
      }else{
        $res['member'] = 'Unknow';
      }
    }
  }
?>
<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8" />
		<title>Product</title>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<link rel="stylesheet" href="../static/css/bootstrap.min.css" />
		<script type="text/javascript" src="../static/js/jquery-2.1.0.min.js"></script>
		<script type="text/javascript" src="../static/js/bootstrap.min.js"></script>
    <style>
			.order {
				border-bottom: 1px solid #eee;
				margin-bottom: 20px;
				padding: 20px;
				box-sizing: border-box;
			}
			
			.product-grid {
				overflow: hidden;
				width: 100%;
				padding: 10px;
				box-sizing: border-box;
			}
			
			.product-grid .image {
				width: ;
			}
			
			.price {
				overflow: hidden;
				width: 100%;
			}
			
			.price .fl {
				float: left;
			}
			
			.price .fr {
				float: right;
			}
		</style>
	</head>

	<body>
		<nav class="navbar navbar-inverse">
			<div class="container-fluid">
				<div class="navbar-header">
					<a class="navbar-brand" href="product.php">Manager</a>
				</div>
				<ul class="nav navbar-nav navbar-right">
						<li class="active">
							<a href="loginout.php">Quit</a>
						</li>
					</ul>
			</div>
		</nav>

		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-3 col-md-2 sidebar">
					<ul class="nav nav-sidebar">
						<li class="active">
							<a href="product.php">Product</a>
						</li>
						<li>
							<a href="order.php">Order</a>
						</li>
						<li>
							<a href="user.php">User</a>
						</li>
					</ul>
				</div>
				<div class="col-sm-9 col-md-10 main">
          <div class="row order">
						<div class="col-sm-3">
							<div class="blog-header">
								<h1 class="blog-title">Order Code:<?php echo($res['order_code']); ?></h1>
								<p class="lead blog-description">Order Status:<?php
                  if($res['status'] == 0){
                    echo('Waiting');
                  }else if($res['status'] == 1){
                    echo('Cooking');
                  }else if($res['status'] == 2){
                    echo('Finish');
                  }else if($res['status'] == 3){
                    echo('Cancel');
                  }
                ?></p>
                <p class="lead blog-description">User:<?php echo($res['member']); ?></p>
							</div>
							<div class="product">
								<p class="text-muted d-none d-sm-block">Time:<?php echo($res['create_time']); ?></p>
								<div class="price">Pay Moneys:$<?php echo($res['pay_moneys']); ?></div>
							</div>
						</div>
						<div class="col-sm-9 productlist">
							<h2 class="sub-header">Product</h2>
              <?php
									foreach($orderlist as $key=>$val)
									{
								?>
							<div class="row product-grid">
								<div class="col-sm-3 product-image">
									<img class="pic-1 img-responsive" src="<?php echo($val['product_cover'] ? $val['product_cover'] : '/upload/none.png'); ?>">
								</div>
								<div class="col-sm-9 product-content">
									<h3 class="title"><?php echo($val['product_name']); ?></h3>
									<div class="price">
										<p class="fl">Count:<?php echo($val['product_count']); ?></p>
										<p class="fr">Price:$<?php echo($val['product_price']); ?></p>
									</div>
								</div>
							</div>
              <?php
									}
								?>
						</div>
					</div>
          <div class="row order">
						<div class="col-sm-12">
							<form action="editorder.php" method="post" enctype="multipart/form-data">
								<input type="hidden" name="order_id" value="<?php echo($id); ?>">
                <div class="form-group">
									<label>Order Status</label>
                  <select class="form-control" name="status">
                    <option value='0' <?php echo($res['status'] == 0 ? 'selected="selected"' : ''); ?>>Waiting</option>
                    <option value='1' <?php echo($res['status'] == 1 ? 'selected="selected"' : ''); ?>>Cooking</option>
                    <option value='2' <?php echo($res['status'] == 2 ? 'selected="selected"' : ''); ?>>Finish</option>
                    <option value='3' <?php echo($res['status'] == 3 ? 'selected="selected"' : ''); ?>>Cancel</option>
                  </select>
								</div>
                <div class="form-group">
									<span style="color:#ff4444;">
									<?php
										echo($err);
									?>
									</span>
								</div>
								<button type="submit" class="btn btn-primary">Submit</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>