<?php
  session_start();
	require '../db/db.php';
  if(!isset($_SESSION['login'])){
		header('location:/admin/login.php');
		exit();
	}

	//query product
	$db = Db::getInstance();
	$list = $db->getData('sys_2022_11_order',['order_id', 'order_code', 'member_id', 'status', 'pay_moneys','create_time']," order by order_id desc");
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>User</title>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<link rel="stylesheet" href="../static/css/bootstrap.min.css" />
		<script type="text/javascript" src="../static/js/jquery-2.1.0.min.js"></script>
		<script type="text/javascript" src="../static/js/bootstrap.min.js"></script>
	</head>

	<body>
		<nav class="navbar navbar-inverse">
			<div class="container-fluid">
				<div class="navbar-header">
					<a class="navbar-brand" href="product.php">Manager</a>
				</div>
				<ul class="nav navbar-nav navbar-right">
						<li class="active">
							<a href="/../index.php">Quitter</a>
						</li>
					</ul>
			</div>
		</nav>

		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-3 col-md-2 sidebar">
					<ul class="nav nav-sidebar">
						<li class="active">
							<a href="product.php">Produit</a>
						</li>
						<li>
							<a href="order.php">Command</a>
						</li>
						<li>
							<a href="user.php">Client</a>
						</li>
					</ul>
				</div>
				<div class="col-sm-9 col-md-10 main">
					<h2 class="sub-header">Command</h2>
					<div class="table-responsive">
						<table class="table table-striped">
							<thead>
								<tr>
									<th>ID</th>
									<th>numero<command></th>
									<th>Etat</th>
									<th>total</th>
									<th>Temp</th>
									<th>Opt</th>
								</tr>
							</thead>
							<tbody>
							<?php
									foreach($list as $key=>$val)
									{
								?>
									<tr>
										<td>
											<?php echo($val['order_id']) ?>
										</td>
										<td>
											<?php echo($val['order_code']) ?>
										</td>
										<td>
											<?php
												if($val['status'] == 0){
													echo('Waiting');
												}else if($val['status'] == 1){
													echo('Cooking');
												}else if($val['status'] == 2){
													echo('Finish');
												}else if($val['status'] == 3){
													echo('Cancel');
												}
											?>
										</td>
										<td>
											<?php echo($val['pay_moneys']) ?>
										</td>
										<td>
											<?php echo($val['create_time']) ?>
										</td>
										<td>
											<a class="btn btn-link" href="<?php echo('editorder.php?id='.$val['order_id']) ?>">Detail</a>
										</td>
									</tr>
								<?php
									}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</body>

</html>