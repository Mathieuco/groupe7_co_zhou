CREATE DATABASE  IF NOT EXISTS `sys_test` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `sys_test`;
-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: localhost    Database: sys_test
-- ------------------------------------------------------
-- Server version	5.7.27-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `sys_2022_11_cart`
--

DROP TABLE IF EXISTS `sys_2022_11_cart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_2022_11_cart` (
  `cart_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `member_id` bigint(20) DEFAULT NULL,
  `product_id` bigint(20) DEFAULT NULL,
  `count` int(4) DEFAULT '0',
  PRIMARY KEY (`cart_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_2022_11_cart`
--

LOCK TABLES `sys_2022_11_cart` WRITE;
/*!40000 ALTER TABLE `sys_2022_11_cart` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_2022_11_cart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_2022_11_member`
--

DROP TABLE IF EXISTS `sys_2022_11_member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_2022_11_member` (
  `member_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `account` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `type` int(4) DEFAULT '0',
  PRIMARY KEY (`member_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_2022_11_member`
--

LOCK TABLES `sys_2022_11_member` WRITE;
/*!40000 ALTER TABLE `sys_2022_11_member` DISABLE KEYS */;
INSERT INTO `sys_2022_11_member` VALUES (1,'admin','123456789',0),(4,'test1','123456',1),(5,'test2','123456',1);
/*!40000 ALTER TABLE `sys_2022_11_member` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_2022_11_order`
--

DROP TABLE IF EXISTS `sys_2022_11_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_2022_11_order` (
  `order_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `order_code` varchar(200) DEFAULT NULL,
  `member_id` bigint(20) DEFAULT NULL,
  `status` int(4) DEFAULT '0',
  `pay_moneys` decimal(18,2) DEFAULT '0.00',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_2022_11_order`
--

LOCK TABLES `sys_2022_11_order` WRITE;
/*!40000 ALTER TABLE `sys_2022_11_order` DISABLE KEYS */;
INSERT INTO `sys_2022_11_order` VALUES (1,'202211145',3,1,10.00,'2022-11-22 05:26:35'),(2,'2022112256253',3,0,20.00,'2022-11-22 07:26:07'),(3,'2022112208804',3,0,20.00,'2022-11-22 07:26:51'),(4,'2022112251494',3,0,30.00,'2022-11-22 07:36:50'),(5,'2022112205514',5,1,40.00,'2022-11-22 07:38:21');
/*!40000 ALTER TABLE `sys_2022_11_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_2022_11_order_detail`
--

DROP TABLE IF EXISTS `sys_2022_11_order_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_2022_11_order_detail` (
  `order_detail_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `order_id` bigint(20) DEFAULT NULL,
  `product_id` bigint(20) DEFAULT NULL,
  `product_name` varchar(200) DEFAULT NULL,
  `product_cover` varchar(200) DEFAULT NULL,
  `product_count` int(4) DEFAULT '0',
  `product_price` decimal(18,2) DEFAULT '0.00',
  PRIMARY KEY (`order_detail_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COMMENT='订单明细';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_2022_11_order_detail`
--

LOCK TABLES `sys_2022_11_order_detail` WRITE;
/*!40000 ALTER TABLE `sys_2022_11_order_detail` DISABLE KEYS */;
INSERT INTO `sys_2022_11_order_detail` VALUES (1,1,1,'dsadasda',NULL,1,12.00),(2,2,6,'145645',NULL,1,10.00),(3,2,3,'test',NULL,1,10.00),(4,3,6,'145645',NULL,1,10.00),(5,3,3,'test',NULL,1,10.00),(6,4,6,'145645',NULL,1,10.00),(7,4,3,'test',NULL,2,10.00),(8,5,6,'145645',NULL,3,10.00),(9,5,3,'test',NULL,1,10.00);
/*!40000 ALTER TABLE `sys_2022_11_order_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_2022_11_product`
--

DROP TABLE IF EXISTS `sys_2022_11_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_2022_11_product` (
  `product_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `product_name` varchar(200) DEFAULT NULL,
  `spec` varchar(100) DEFAULT NULL,
  `product_cover` varchar(200) DEFAULT NULL,
  `product_detail` varchar(1000) DEFAULT NULL,
  `moneys` decimal(18,2) DEFAULT '0.00',
  `classify` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_2022_11_product`
--

LOCK TABLES `sys_2022_11_product` WRITE;
/*!40000 ALTER TABLE `sys_2022_11_product` DISABLE KEYS */;
INSERT INTO `sys_2022_11_product` VALUES (1,'test','dsds,dsds','https://img0.baidu.com/it/u=1472391233,99561733&fm=253&app=138&size=w931&n=0&f=JPEG&fmt=auto?sec=1669222800&t=882a4c318b3adaea382a695d32ce78d4','ddasdadada',10.00,'staple food'),(3,'test','dsds,dsds',NULL,'ddasdadada',10.00,'staple food'),(6,'145645','dadadasd,dadads','/upload/1669064102.jpg','dadasdada',10.00,'staple food'),(7,'test product','dsdsd','/upload/1669073959.jpg','dsdsdad',20.00,'snacks');
/*!40000 ALTER TABLE `sys_2022_11_product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'sys_test'
--

--
-- Dumping routines for database 'sys_test'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-11-22  8:13:01
