<?php
  session_start();
	require 'db/db.php';
  if(!isset($_SESSION['user'])){
    header('location:/login.php');
    exit();
  }

	//query product
	$db = Db::getInstance();

	//query user
	$cartNumber = 0;
	$cartMoneys = 0;
  $islogin = true;
  $name = $_SESSION['user']['account'];
  $cart = $db->getList('select a.*,b.moneys from sys_2022_11_cart a left join sys_2022_11_product b on a.product_id = b.product_id where a.member_id = '.$_SESSION['user']['member_id']);
  foreach($cart as $key=>$val)
  {
    $cartNumber += $val['count'];
    $cartMoneys += $val['count'] * $val['moneys'];
  }
  $orderlist = $db->getData('sys_2022_11_order',['order_id', 'order_code', 'member_id', 'status', 'pay_moneys','create_time']," where member_id = ".$_SESSION['user']['member_id']." order by order_id desc");
	for($i = 0;$i < count($orderlist);$i++){
		$orderdetail = $db->getData('sys_2022_11_order_detail',['order_detail_id', 'order_id', 'product_id', 'product_name', 'product_cover','product_count','product_price']," where order_id = ".$orderlist[$i]['order_id']);
    $orderlist[$i]['detail'] = $orderdetail;
	}
?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>Index</title>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<link rel="stylesheet" href="static/css/bootstrap.min.css" />
		<script type="text/javascript" src="static/js/jquery-2.1.0.min.js"></script>
		<script type="text/javascript" src="static/js/bootstrap.min.js"></script>
		<style>
			.order{
				border-bottom: 1px solid #eee;
				margin-bottom: 20px;
			}
			.product-grid{
				overflow: hidden;
				width: 100%;
				padding: 10px;
				box-sizing: border-box;
			}
			.product-grid .image{
				width: ;
			}
			.price {
				overflow: hidden;
				width: 100%;
			}
			.price .fl{
				float: left;
			}
			.price .fr{
				float: right;
			}
		</style>
		<link rel="stylesheet" href="static/css/base.css" />
	</head>

	<body>
		<div class="container">
			<!-- Static navbar -->
			<nav class="navbar navbar-default">
				<div class="container-fluid">
					<div class="navbar-header">
						<a class="navbar-brand" href="index.php">Restaurant</a>
					</div>
					<div id="navbar" class="navbar-collapse collapse">
						<ul class="nav navbar-nav">
							<li>
								<a href="index.php">Home</a>
							</li>
							<li class="active">
								<a href="order.php">Order</a>
							</li>
							<li>
								<a href="admin/login.php">Manager</a>
							</li>
						</ul>
						<?php
							if(!$islogin){
						?>
						<ul class="nav navbar-nav navbar-right">
							<li class="active">
								<a href="login.php">Login</a>
							</li>
							<li>
								<a href="signin.php">Sign Up</a>
							</li>
						</ul>
						<?php
							}else{
						?>
							<ul class="nav navbar-nav navbar-right">
								<li>
									<a href="loginout.php">Login Out</a>
								</li>
							</ul>
						<?php
							}
						?>
					</div>
					<!--/.nav-collapse -->
				</div>
				<!--/.container-fluid -->
			</nav>
			<div class="row row-offcanvas row-offcanvas-right">
				<div class="site-branding-area">
					<div class="container">
						<div class="row">
							<div class="col-sm-6">
								<div class="logo">
									<h1>Hello<?php echo($name ? ','.$name : '') ?></h1>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="shopping-item">
									<a href="cart.php">Cart - <span class="cart-amunt">$
									<?php echo($cartMoneys) ?>
									</span><span class="product-count"><?php echo($cartNumber) ?></span></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row row-offcanvas row-offcanvas-right">
				<div class="col-xs-12 col-sm-12">
          <?php
            foreach($orderlist as $key=>$val)
            {
          ?>
					<div class="row order">
						<div class="col-sm-3">
							<div class="product">
								<h4 class="name">Order Code:<?php echo($val['order_code']); ?></h4>
								<p class="text-muted d-none d-sm-block">Time:<?php echo($val['create_time']); ?></p>
								<div class="price">Pay Moneys:$<?php echo($val['pay_moneys']); ?></div>
								<div class="price">Order Status:<?php
                  if($val['status'] == 0){
                    echo('Waiting');
                  }else if($val['status'] == 1){
                    echo('Cooking');
                  }else if($val['status'] == 2){
                    echo('Finish');
                  }else if($val['status'] == 3){
                    echo('Cancel');
                  }
                ?></div>
							</div>
						</div>
						<div class="col-sm-9 productlist">
              <?php
                foreach($val['detail'] as $key1=>$val1)
                {
              ?>
							<div class="row product-grid">
								<div class="col-sm-3 product-image">
									<a href="#" class="image"><img class="pic-1 img-responsive" src="<?php echo($val1['product_cover'] ? $val1['product_cover'] : '/upload/none.png'); ?>"></a>
								</div>
								<div class="col-sm-9 product-content">
									<h3 class="title"><?php echo($val1['product_name']); ?></h3>
									<div class="price">
										<p class="fl">Count:<?php echo($val1['product_count']); ?></p>
										<p class="fr">Price:$<?php echo($val1['product_price']); ?></p>
									</div>
								</div>
							</div>
              <?php
                }
              ?>
						</div>
					</div>
          <?php
            }
          ?>
				</div>
				<!--/row-->

			</div>
			<!--/row-->
		</div>
		<!--/.container-->
	</body>

</html>